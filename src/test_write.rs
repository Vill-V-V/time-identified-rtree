use mylib::rtree_tiIBR;

//mod rtree_tiIBR;
// mod tirtree;
//
use rand;
use csv::{Reader, Writer, Error as CsvError};

use std::collections::VecDeque;
use std::time::{Duration, Instant};
use std::fmt::Display;
use std::fs::File;
use std::io::prelude::*;
use std::io::{Write, BufReader, BufRead, Error};
use crate::ALLOCATOR;

use crossbeam;
use crossbeam_channel;
use crossbeam_channel::bounded;

fn read_csv_file(file_path:&String,  file_name:&String, vec: &mut VecDeque<(u64,f64,f64)>) -> Result<(),CsvError> {
    let mut reader = Reader::from_path(format!("{}{}",file_path, file_name))?;
    for rec in reader.records(){
        let record = rec?;
        let tuid = &record[0];
        let lat = &record[1];
        let lon = &record[2];
        vec.push_back((
            tuid.parse::<u64>().unwrap(),
            lat.parse::<f64>().unwrap(),
            lon.parse::<f64>().unwrap()
        ));
    }
    Ok(())
}

pub fn random_tiRIBR_25k_1() -> Result<(),CsvError>{
    let configs = rtree_tiIBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 30_000,
        bin_volumn: 30_000,
        collection_volumn: 24250,
        buffer_volumn: 250
    };
    let file_path = "./data/sample-data/random/".to_string();
    let output_record_file_name = "./data/log-write/random_tiRIBR_25k_1.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    let mut rt:rtree_tiIBR::Rtree<2> = rtree_tiIBR::Rtree::new(configs);

    let mut write_cnt = 0;
    for _cnt in 1..101 {
        let file_name = format!("random-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(1000_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            let temp_node = rtree_tiIBR:: Node::new(
                rtree_tiIBR::Rect::new(rtree_tiIBR::Point::new([_t.1,_t.2]), rtree_tiIBR::Point::new([_t.1,_t.2])),
                Vec::with_capacity(0),
                0,
                false,
                0,
                _t.0
            );
            let res = rt.insert(temp_node);
            if let Some(ser) = res {
                write_cnt += 1;
                let path = format!("./data/index/test-output-{:}.json", write_cnt);
                    
                let mut output = File::create(path)?;
                write!(output, "{}", ser)?;
    
            }
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: random tiRIBR 25k 1%.");
    Ok(())
}

pub fn crossbeam_tiRIBR(configs: rtree_tiIBR::ConfigParams, dataset: String, suffix: String) -> Result<(),CsvError>{
    // file names prepare
    let mut files: Vec<String> = vec![];
    let file_path = format!("./data/sample-data/{}/", dataset).to_string();
    let output_record_file_name = format!("./data/log-write/{}_tiRIBR_{}.csv", dataset, suffix).to_string();
    let output_file_path = format!("./data/index/{}/{}/",dataset,suffix).to_string();
    for _cnt in 0..101 {
        let file_name = format!("{}-{}.csv", dataset,_cnt).to_string();
        files.push(file_name);
    }    

    
    let start = Instant::now();
    // parallel process begin
    let n_workers = 5;
    let _ = crossbeam::scope(|s| {
        
        for _w_i in 0..n_workers {
            let the_configs = configs.clone();
            let mut the_files:Vec<String>= vec![];
            for _f in files.clone() {
                the_files.push(_f);
            }
            let the_file_path = file_path.clone();
            let the_output_file_path = output_file_path.clone();
            let the_dataset = dataset.clone();

            s.spawn(move |_| {
                
                let mut rt:rtree_tiIBR::Rtree<2> = rtree_tiIBR::Rtree::new(the_configs);
                let mut write_cnt = 0;
                for _c in 0..20 {
                    let file_id = _c*5+ _w_i+1;
                    let mut tuple_vec = VecDeque::with_capacity(1000_000);
                    
                    read_csv_file(&the_file_path, &the_files[file_id], &mut tuple_vec);

                    for _i in 0..tuple_vec.len() {
                        let _t = tuple_vec[_i];
                        let temp_node = rtree_tiIBR:: Node::new(
                            rtree_tiIBR::Rect::new(rtree_tiIBR::Point::new([_t.1,_t.2]), rtree_tiIBR::Point::new([_t.1,_t.2])),
                            Vec::with_capacity(0),
                            0,
                            false,
                            0,
                            _t.0
                        );
                        let res = rt.insert(temp_node);

                        if let Some(ser) = res {
                            write_cnt += 1;
                            let path = format!("{}{}-{}-{}.json", the_output_file_path,the_dataset,_w_i,write_cnt);
                                
                            let mut output = File::create(path).unwrap();
                            write!(output, "{}", ser);
                
                        }
                    }
                }
            });
        } 
    });
    // parallel process end
    let duration = format!("{:?}",start.elapsed().as_nanos());
    let mut wtr = Writer::from_path(output_record_file_name)?;
    wtr.write_record(&["total_time".to_string(),duration])?;     
    wtr.flush()?;
    Ok(())
}
pub fn crossbeam_random_tiRIBR_25k_25() {
    let configs = rtree_tiIBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 30_000,
        bin_volumn: 30_000,
        collection_volumn: 18750,
        buffer_volumn: 6250
    };
    let dataset = "random".to_string();
    let suffix = "25k-25".to_string();
    crossbeam_tiRIBR(configs, dataset, suffix);
}

pub fn crossbeam_random_tiRIBR_1M_25() {
    let configs = rtree_tiIBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 1000_000,
        bin_volumn: 1000_000,
        collection_volumn: 750_000,
        buffer_volumn: 250_000
    };
    let dataset = "random".to_string();
    let suffix = "1M-25".to_string();
    crossbeam_tiRIBR(configs, dataset, suffix);
}
pub fn crossbeam_ADSB_tiRIBR_25k_25() {
    let configs = rtree_tiIBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 30_000,
        bin_volumn: 30_000,
        collection_volumn: 18750,
        buffer_volumn: 6250
    };
    let dataset = "ADSB".to_string();
    let suffix = "25k-25".to_string();
    crossbeam_tiRIBR(configs, dataset, suffix);
}

pub fn crossbeam_ADSB_tiRIBR_1M_25() {
    let configs = rtree_tiIBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 1000_000,
        bin_volumn: 1000_000,
        collection_volumn: 750_000,
        buffer_volumn: 250_000
    };
    let dataset = "ADSB".to_string();
    let suffix = "1M-25".to_string();
    crossbeam_tiRIBR(configs, dataset, suffix);
}

pub fn crossbeam_AIS_tiRIBR_25k_25() {
    let configs = rtree_tiIBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 30_000,
        bin_volumn: 30_000,
        collection_volumn: 18750,
        buffer_volumn: 6250
    };
    let dataset = "AIS".to_string();
    let suffix = "25k-25".to_string();
    crossbeam_tiRIBR(configs, dataset, suffix);
}

pub fn crossbeam_AIS_tiRIBR_1M_25() {
    let configs = rtree_tiIBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 1000_000,
        bin_volumn: 1000_000,
        collection_volumn: 750_000,
        buffer_volumn: 250_000
    };
    let dataset = "AIS".to_string();
    let suffix = "1M-25".to_string();
    crossbeam_tiRIBR(configs, dataset, suffix);
}