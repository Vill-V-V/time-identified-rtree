use std::cmp::Ordering;
use std::collections::VecDeque;

use serde::{Deserialize, Serialize};
use serde_big_array::BigArray;


/// Point and Rectangle
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Point<const D:usize>{
    #[serde(with = "BigArray")]
    coords: [f64;D]
}
#[derive(Clone, Deserialize, Serialize)]
pub struct Rect<const D:usize>{
    min: Point<D>,
    max: Point<D>
}

impl <const D:usize> Point<D> {
    pub fn new(array:[f64;D]) -> Point<D>{
        Point{coords:array}
    }
}
impl <const D:usize> Rect<D>{
    pub fn new_empty_rect() -> Rect<D>{
        Rect{ min: Point::new([0.0;D]), max: Point::new([0.0;D])}
    }
    pub fn new(p1:Point<D>,p2:Point<D>) -> Rect<D>{
        let mut min = [0.0;D];
        let mut max = [0.0;D];
        for _i in 0..D{
            if p1.coords[_i] <= p2.coords[_i] {
                min[_i] = p1.coords[_i];
                max[_i] = p2.coords[_i];
            }
            else{
                min[_i] = p2.coords[_i];
                max[_i] = p1.coords[_i];
            }
        }
        Rect{ min: Point::new(min), max: Point::new(max)}
    }
    pub fn expand(&mut self, rect: &Rect<D>){
       for _i in 0..D{
            if rect.min.coords[_i] < self.min.coords[_i] {
                self.min.coords[_i] = rect.min.coords[_i];
            } 
            if rect.max.coords[_i] > self.max.coords[_i] {
                self.max.coords[_i] = rect.max.coords[_i];
            }
       } 
    }
    pub fn area(&self) -> f64 {
        if D==0 {
            return 0.0;
        }
        let mut area = self.max.coords[0]-self.min.coords[0];
        for _i in 1..D {
            area = area * (self.max.coords[_i]-self.min.coords[_i]);
        }
        area
    }
    pub fn unioned_area(&self, rect: &Rect<D>) -> f64 {
        let mut r = self.clone();
        r.expand(rect);
        r.area()
    }
    pub fn largest_axis(&self) -> usize {
        if D == 0 {
            return 0;
        }
        let mut axis = 0;
        let mut length = self.max.coords[0] - self.min.coords[0];
        for _i in 1..D {
            let temp_len = self.max.coords[_i] - self.min.coords[_i];
            if temp_len > length {
                axis = _i;
                length = temp_len;
            }
        }
        axis
    }
    pub fn cmp_by_axis_min(rect1: &Rect<D>, rect2: &Rect<D>, axis:usize) -> Ordering{
        if rect1.min.coords[axis] < rect2.min.coords[axis]{
            return Ordering::Less;
        }
        else if rect1.min.coords[axis] > rect2.min.coords[axis]{
            return Ordering::Greater;
        }
        else{
            return Ordering::Equal;
        }
    }
    pub fn cmp_by_axis_max(rect1: &Rect<D>, rect2: &Rect<D>, axis:usize) -> Ordering{
        if rect1.max.coords[axis] < rect2.max.coords[axis]{
            return Ordering::Less;
        }
        else if rect1.max.coords[axis] > rect2.max.coords[axis]{
            return Ordering::Greater;
        }
        else{
            return Ordering::Equal;
        }
    }
    pub fn contains(&self, rect: &Rect<D>) -> bool {
        if D == 0 {
            return false;
        }
        for i in 0..D {
            if rect.min.coords[i] < self.min.coords[i] || rect.max.coords[i] > self.max.coords[i] {
                return false;
            }
        }
        true
    }
    pub fn intersects(&self, rect: &Rect<D>) -> bool {
        if D == 0 {
            return false;
        }
        for i in 0..D {
            if rect.min.coords[i] > self.max.coords[i] || rect.max.coords[i] < self.min.coords[i] {
                return false;
            }
        }
        true
    }
    pub fn equals(&self, rect: &Rect<D>) -> bool{
        let mut flag = true;
        if D > 0 {
            for i in 0..D {
                if rect.min.coords[i] != self.min.coords[i] || rect.max.coords[i] != self.max.coords[i]{
                    flag = false;
                }
            }
        }
        flag
    }
}

// R-tree and Node
#[derive(Clone, Deserialize, Serialize)]
pub struct Node<const D:usize>{
    r: Rect<D>,
    chidren: Vec<usize>, // the index of child nodes in the node array of the tree. 
    height: usize, // leaf node's height is 0
    is_freed: bool,
    node_id: usize,
    tuid: u64
}

impl <const D: usize> Node<D> {
    pub fn new(r : Rect<D>, c_vec: Vec<usize>, height: usize, is_freed: bool, node_id: usize, tuid: u64) -> Node<D> {
        Node{r:r, chidren: c_vec, height:height,is_freed: is_freed, node_id: node_id, tuid: tuid}
    } 
    pub fn free(&mut self) {
        self.is_freed = true;
    }
    pub fn is_freed(&self) -> bool{
        self.is_freed
    }
}
#[derive(Clone, Deserialize, Serialize)]
pub struct ConfigParams{
    pub min_items: usize,
    pub max_items: usize,
    pub capacity: usize,
    pub bin_volumn: usize,
    pub collection_volumn: usize,
    pub buffer_volumn: usize
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Rtree<const D:usize> {
    configs: ConfigParams,
    node_vec: Vec<Node<D>>,
    recycle_bin: Vec<usize>,
    root_id: usize,
    collection: VecDeque<Node<D>>,
    insert_counter: usize
    //pub remove_recurse_cnt: usize,
    //pub insert_recurse_cnt: usize
}

impl <const D:usize> Rtree<D> {
    pub fn new(configs: ConfigParams) -> Rtree<D> {
        let mut node_vec:Vec<Node<D>> = Vec::with_capacity(configs.capacity);
        node_vec.push(Node::new(Rect::new_empty_rect(),
                               Vec::with_capacity(1),
                                1000, 
                                false, 
                                0,
                                0)); // empty node, to occupy the usize index "0"
        node_vec.push(Node::new(Rect::new_empty_rect(),
                                Vec::with_capacity(configs.max_items),
                                1, 
                                false, 
                                1,
                                0));
        let recycle_bin:Vec<usize> = Vec::with_capacity(configs.bin_volumn);
        let root_index = 1;
        let collection: VecDeque<Node<D>> = VecDeque::with_capacity(configs.collection_volumn+configs.buffer_volumn);

        Rtree{
            configs: configs,
            node_vec: node_vec,
            recycle_bin: recycle_bin,
            root_id: root_index,
            collection: collection,
            insert_counter: 0
            //remove_recurse_cnt: 0,
            //insert_recurse_cnt: 0
        }
    }
    pub fn new_empty_node(&self, is_leaf: bool) -> Node<D> {
        let mut vec_cap = 0;
        let mut height = 0;
        if !is_leaf{
            vec_cap = self.configs.max_items+1;
            height = 1000; 
        }
        Node:: new( Rect::new_empty_rect(),
                    Vec::with_capacity(vec_cap),
                    height, 
                    false,
                    0,
                    0
                    )
    }
    fn remove_node(&mut self, node_index: usize) {
        if !self.node_vec[node_index].is_freed(){
            self.node_vec[node_index].free();
            self.node_vec[node_index].chidren.clear();
            self.recycle_bin.push(node_index);
        }
    }
    fn insert_node(&mut self, node: &Node<D>) -> usize {
        let i = self.recycle_bin.pop();
        let mut node_index = 0;
        match i {
            Some(x) => {
                self.node_vec[x] = node.clone();
                node_index = x;
            },
            _ => {
                self.node_vec.push(node.clone());
                node_index = self.node_vec.len()-1;
            }
        }
        self.node_vec[node_index].node_id = node_index;
        node_index
    }
    fn choose_brunch(&self, p_i:usize, node: &Node<D>) -> usize {
        if D==0 || self.node_vec[p_i].chidren.is_empty(){
            return 0;
        }
        let mut ans_i: usize = self.node_vec[p_i].chidren[0];
        if self.node_vec[ans_i].height ==0 {
            return 0;
        }
        let mut ans_area = self.node_vec[ans_i].r.area();
        let mut ans_ex_area = self.node_vec[ans_i].r.unioned_area(&node.r) - ans_area;
        let v_len = self.node_vec[p_i].chidren.len();
        for _i in  1..v_len{
            let temp_i = self.node_vec[p_i].chidren[_i];
            let temp_area = self.node_vec[temp_i].r.area();
            let temp_ex_area = self.node_vec[temp_i].r.unioned_area(&node.r) - temp_area;
            if temp_ex_area < ans_ex_area || ( temp_ex_area == ans_ex_area && temp_area < ans_area) {
                ans_i = temp_i;
                ans_area = temp_area;
                ans_ex_area = temp_ex_area;
            }
        }
        ans_i 
    }
    fn recalculate_node(&mut self, p_i:usize){
        if self.node_vec[p_i].height > 0 {
            let temp_len = self.node_vec[p_i].chidren.len();
            if temp_len > 0 {
                //let mut r = Rect::new_empty_rect();
                let temp_i = self.node_vec[p_i].chidren[0];
                let mut r = self.node_vec[temp_i].r.clone();
                for _i in 1..temp_len{
                    let q_i = self.node_vec[p_i].chidren[_i];
                    r.expand(&self.node_vec[q_i].r);
                }
                self.node_vec[p_i].r = r.clone();
            }
            
        }
    }
    fn node_children_sort_by_axis_min(&self, v:&mut Vec<usize>, axis:usize) {
        let temp_len = v.len(); 
        let mut temp_vec : Vec<(usize, Rect<D>)>= Vec::with_capacity(temp_len);
        for _i in 0..temp_len{
            let q_i = v[_i];
            temp_vec.push((q_i,self.node_vec[q_i].r.clone()));
        }
        temp_vec.sort_by(|a,b| 
                                            Rect::cmp_by_axis_min(
                                                &a.1,
                                                &b.1,
                                                axis
                                            ));
        for _i in 0..temp_len{
             v[_i] = temp_vec[_i].0;
        }
    }
    fn node_children_sort_by_axis_max(&self, v:&mut Vec<usize>, axis:usize) {
        let temp_len = v.len(); 
        let mut temp_vec : Vec<(usize, Rect<D>)>= Vec::with_capacity(temp_len);
        for _i in 0..temp_len{
            let q_i = v[_i];
            temp_vec.push((q_i,self.node_vec[q_i].r.clone()));
        }
        temp_vec.sort_by(|a,b| 
                                            Rect::cmp_by_axis_max(
                                                &a.1,
                                                &b.1,
                                                axis
                                            ));
        for _i in 0..temp_len{
             v[_i] = temp_vec[_i].0;
        }
    }
    fn split_node(&mut self, p_i: usize) -> Node<D>{
        let mut right_brunch = self.new_empty_node(false);
        let axis = self.node_vec[p_i].r.largest_axis();
        right_brunch.height = self.node_vec[p_i].height;
        let temp_len = self.node_vec[p_i].chidren.len();
        let mut lc: Vec<usize> = Vec::with_capacity(temp_len);
        let mut rc: Vec<usize> = Vec::with_capacity(temp_len);
        for _i in self.node_vec[p_i].chidren.clone(){
            let temp_min = self.node_vec[_i].r.min.coords[axis] - self.node_vec[p_i].r.min.coords[axis];
            let temp_max = -(self.node_vec[_i].r.max.coords[axis] - self.node_vec[p_i].r.max.coords[axis]);
            if temp_min < temp_max{
                lc.push(_i)
            }else{
                rc.push(_i);
            }
        }

        if lc.len() < self.configs.min_items{
            self.node_children_sort_by_axis_min(&mut rc, axis);
            rc.reverse();
            while lc.len() < self.configs.min_items{
                lc.push(rc.pop().unwrap());
            }
        }
        else if rc.len() < self.configs.min_items{
            self.node_children_sort_by_axis_max(&mut lc, axis);
            while rc.len() < self.configs.min_items{
                rc.push(lc.pop().unwrap());
            }
        }
        self.node_vec[p_i].chidren = lc.clone();
        right_brunch.chidren = rc.clone();
        right_brunch
    }

    fn insert_recurse(&mut self, p_i: usize, l_i: usize) {
        // self.insert_recurse_cnt += 1;
       if self.node_vec[p_i].height == 1 && !self.node_vec[p_i].is_freed{
            // let q_i = self.insert_node(node);
            if self.node_vec[p_i].chidren.is_empty(){
                self.node_vec[p_i].r = self.node_vec[l_i].r.clone();    
            }else{
                let temp_r = self.node_vec[l_i].r.clone();
                self.node_vec[p_i].r.expand(&temp_r);
            }
            self.node_vec[p_i].chidren.push(l_i);
           
            
       }
       else{
            let q_i = self.choose_brunch(p_i, &self.node_vec[l_i]);
            self.insert_recurse(q_i, l_i);
            if self.node_vec[q_i].chidren.len() > self.configs.max_items {
                let right_brunch = self.split_node(q_i);
                let right_i = self.insert_node(&right_brunch);
                self.recalculate_node(q_i);
                self.recalculate_node(right_i);
                
                self.node_vec[p_i].chidren.push(right_i);
            }
            let temp_r = self.node_vec[l_i].r.clone();
            self.node_vec[p_i].r.expand(&temp_r);
       }
    }
    fn insert_in_node_vec(&mut self, l_i:usize){
        let r_i = self.root_id;
        
        self.insert_recurse(r_i, l_i);

        if self.node_vec[r_i].chidren.len() > self.configs.max_items {
            let mut new_root_node = self.new_empty_node(false);
            new_root_node.height = self.node_vec[r_i].height+1;
            let right_brunch = self.split_node(r_i);
            let right_i = self.insert_node(&right_brunch);
            self.recalculate_node(r_i);
            self.recalculate_node(right_i);

            new_root_node.chidren.push(r_i);
            new_root_node.chidren.push(right_i);

            self.root_id = self.insert_node(&new_root_node);
            self.recalculate_node(self.root_id);
        }
    }
    pub fn insert(&mut self, node: Node<D>) -> Option<String> {
        let l_i = self.insert_node(&node);
        self.insert_in_node_vec(l_i);

        let mut temp_node = node.clone();
        temp_node.node_id = l_i;
        self.collection.push_front(temp_node);
        
        let mut Op: Option<String> = None;
        self.insert_counter += 1;
        if self.insert_counter >= self.configs.collection_volumn+self.configs.buffer_volumn{
            self.insert_counter = 0;
            Op = Some(self.index_serialize());
        }
        
        if self.collection.len() >= self.configs.collection_volumn + self.configs.buffer_volumn {
            while self.collection.len() > self.configs.collection_volumn {
                let n = self.collection.pop_back().unwrap();
                self.remove(&n);
            }
        }
        
        Op
    }
    fn pick_leaf_nodes(&mut self, p_i: usize, leaf_nodes: &mut Vec<usize>) {
        if !self.node_vec[p_i].is_freed(){
            if self.node_vec[p_i].height == 0 {
                leaf_nodes.push(p_i);
            }else {
                    while let Some(q_i) = self.node_vec[p_i].chidren.pop() {
                        self.pick_leaf_nodes(q_i, leaf_nodes);
                    }
                    self.remove_node(p_i);
            }
        }
    }
    fn remove_recurse(&mut self, p_i: usize, node: &Node<D>, reinsert_leaf_nodes: &mut Vec<usize>) -> usize{
        // self.remove_recurse_cnt += 1;

        let mut flag = 0; // 0: no change 1: changed 2: underflow
        
        let mut remove_can: Vec<usize> = Vec::with_capacity(self.configs.max_items);

        for _i in 0..self.node_vec[p_i].chidren.len() {
            let q_i = self.node_vec[p_i].chidren[_i];
            if self.node_vec[q_i].r.intersects(&node.r) {
                if self.node_vec[q_i].height == 0 {
                    if self.node_vec[q_i].tuid == node.tuid {
                        self.remove_node(q_i);
                        remove_can.push(_i);
                        // self.node_vec[p_i].chidren.remove(_i);
                        flag = 1;
                        break;
                    }else{
                        continue;
                    }
                }
            
                let temp_flag = self.remove_recurse(q_i, node, reinsert_leaf_nodes);
                if temp_flag > 0 {
                    flag = 1;
                    
                    if temp_flag == 2{ // underflow
                        self.pick_leaf_nodes(q_i, reinsert_leaf_nodes);
                        
                        // self.remove_node(q_i);
                        //self.node_vec[p_i].chidren.remove(_i);
                        remove_can.push(_i);
                    }
                    break;
                }
            }
        }
        while let Some(_x) = remove_can.pop() {
            // the remove order is very important
            self.node_vec[p_i].chidren.remove(_x);
        }
        if flag > 0{
            if self.node_vec[p_i].chidren.len() < self.configs.min_items {
                flag = 2;
            }else{
                self.recalculate_node(p_i);
            }
        }
        flag
    }
    pub fn remove(&mut self, node: &Node<D>) {
    
        let r_i = self.root_id;
        let mut reinsert_leaf_nodes: Vec<usize> = Vec::with_capacity(self.configs.max_items);
        
        let flag = self.remove_recurse(r_i, node, &mut reinsert_leaf_nodes);
        if flag > 0{
            if self.node_vec[r_i].chidren.len() == 1 && self.node_vec[r_i].height > 1 {
                self.root_id = self.node_vec[r_i].chidren[0];
                self.remove_node(r_i);
            }else{
                self.recalculate_node(r_i);
            }
            while let Some(l_i) = reinsert_leaf_nodes.pop(){
                //let _n = self.node_vec[l_i].clone();
                //self.remove_node(l_i);
                self.node_vec[l_i].is_freed = false;
                self.insert_in_node_vec(l_i);
            }
        }
        
    }
    fn search_recurse(&self, p_i: usize, rect: &Rect<D>, vec: &mut Vec<Node<D>>){
        for q_i in self.node_vec[p_i].chidren.clone() {
            if self.node_vec[q_i].r.intersects(rect) {
                if self.node_vec[q_i].height == 0 {
                    vec.push(self.node_vec[q_i].clone());
                }else{
                    self.search_recurse(q_i, rect, vec)
                }
            }
        }
    }
    pub fn search(&self, rect: Rect<D>) -> Vec<Node<D>>{
        let mut output: Vec<Node<D>> = vec![];
        
        let r_i = self.root_id;
        self.search_recurse(r_i, &rect, &mut output);
        output
    }
    pub fn index_serialize(&self)  -> String{
        let ser = serde_json::to_string(self).unwrap();
        return ser;
    }
}


use rand::Rng;
#[test]
fn test_r_tree_insert_remove(){
    let configs = ConfigParams{
        max_items: 10,
        min_items: 4,
        capacity: 1_000,
        bin_volumn: 1_000,
        collection_volumn: 1_00,
        buffer_volumn: 10
    };
    let mut rt: Rtree<2> = Rtree::new(configs);
    let mut rng = rand::thread_rng();
    let mut n_vec = Vec::with_capacity(500);
    for _i in 0..500{
        let _t = (_i+1,rng.gen_range(-90.0..90.0),rng.gen_range(-180.0..180.0));
        let temp_node = Node::new(
            Rect::new(Point::new([_t.1,_t.2]), Point::new([_t.1,_t.2])),
            Vec::with_capacity(0),
            0,
            false,
            0,
            _t.0
        );
        n_vec.push(temp_node);
    }

    for _n in n_vec.clone(){
        
        rt.insert(_n);
    }

    for _n in n_vec.clone(){
        rt.remove(&_n);
    }

    for _n in n_vec.clone(){
        
        rt.insert(_n);
    }
    println!("End!");    
}

#[test]
fn test_building_accuracy(){
    let configs = ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 1_000_000,
        bin_volumn: 1_000_000,
        collection_volumn: 25_000,
        buffer_volumn: 25_0
    };
    let mut rt: Rtree<2> = Rtree::new(configs);
    let mut rng = rand::thread_rng();
    let mut n_vec = Vec::with_capacity(500_000);
    for _i in 0..50_000{
        let _t = (_i+1,rng.gen_range(-90.0..90.0),rng.gen_range(-180.0..180.0));
        let temp_node = Node::new(
            Rect::new(Point::new([_t.1,_t.2]), Point::new([_t.1,_t.2])),
            Vec::with_capacity(0),
            0,
            false,
            0,
            _t.0
        );
        n_vec.push(temp_node);
    }

    for _n in n_vec.clone(){
        
        rt.insert(_n);
    }
    for _i in 0..50{
        rt.remove(&n_vec[_i]);
    }
    let mut flag = true;
    for _i in 1..rt.node_vec.len() {
        if ! rt.node_vec[_i].is_freed{
            let temp_r = rt.node_vec[_i].r.clone();
            rt.recalculate_node(_i);
            let temp_flag =  temp_r.equals(&rt.node_vec[_i].r);
            flag = flag && temp_flag;
            if ! temp_flag {
                let e = _i;
                println!("{}",e);
            }
        }
    }
    assert!(flag);
}

use std::fs::File;
use std::io::prelude::*;
use std::io::{Write, BufReader, BufRead, Error};
#[test]
pub fn test_building_performance() -> Result<(), Error>{
    let configs = ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 1_000_000,
        bin_volumn: 1_000_000,
        collection_volumn: 250_000,
        buffer_volumn: 1
    };
    let mut rt: Rtree<2> = Rtree::new(configs);
    let mut rng = rand::thread_rng();
    let mut n_vec = Vec::with_capacity(1_000_000);
    for _i in 0..1_000_000{
        let _t = (_i+1,rng.gen_range(-90.0..90.0),rng.gen_range(-180.0..180.0));
        let temp_node = Node::new(
            Rect::new(Point::new([_t.1,_t.2]), Point::new([_t.1,_t.2])),
            Vec::with_capacity(0),
            0,
            false,
            0,
            _t.0
        );
        n_vec.push(temp_node);
    }

    for _n in n_vec.clone(){
        
        rt.insert(_n);
    }

    //for _i in 0..100 {
    //    let _a = (_i+1,rng.gen_range(-90.0..90.0),rng.gen_range(-180.0..180.0));
    //    let _b = (_i+1,rng.gen_range(-90.0..90.0),rng.gen_range(-180.0..180.0));
    //    
    //    let temp_rect = Rect::new(Point::new([_a.1,_a.2]), Point::new([_b.1,_b.2]));
    //    let res = rt.search(temp_rect.clone());
    //    println!("{}:  {:?}, {:?}", res.len(), temp_rect.min.coords, temp_rect.max.coords);
    //    if res.len() > 0 {
    //        let mut res_rect = res[0].r.clone();
    //        for i in 1..res.len() {
    //            res_rect.expand(&res[i].r);
    //        }
    //        println!("{:?}, {:?} ",res_rect.min.coords, res_rect.max.coords)
    //    }
    //    
    //}
    //for _i in 0..n_vec.len(){
    //    rt.remove(&n_vec[_i]);
    //}
    //for _i in 0..500_000{
    //    rt.insert(n_vec[_i].clone());
    //}
    //println!("insert_rec:{}",rt.insert_recurse_cnt);
    //println!("remove_rec:{}",rt.remove_recurse_cnt);
    //let ser = serde_json::to_string(&rt).unwrap();
    //
    //let path = "./data/index/test-output.json";
    //    
    //let mut output = File::create(path)?;
    //write!(output, "{}", ser)?;
    
    //let mut file = File::open(path)?;
    //let mut contents = String::new();
    //file.read_to_string(&mut contents)?;
    //let mut deser:Rtree<2> = serde_json::from_str(&contents).unwrap();

    //for _i in 0..n_vec.len(){
    //    deser.remove(&n_vec[_i]);
    //}
    println!("End!");
    Ok(())
}