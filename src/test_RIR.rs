use mylib::rtree_IBR;

//mod rtree_IBR;
// mod tirtree;
//
use rand;
use csv::{Reader, Writer, Error as CsvError};

use std::collections::VecDeque;
use std::time::{Duration, Instant};
use std::fmt::Display;
use std::fs::File;

use crate::ALLOCATOR;

fn read_csv_file(file_path:&String,  file_name:&String, vec: &mut VecDeque<(u64,f64,f64)>) -> Result<(),CsvError> {
    let mut reader = Reader::from_path(format!("{}{}",file_path, file_name))?;
    for rec in reader.records(){
        let record = rec?;
        let tuid = &record[0];
        let lat = &record[1];
        let lon = &record[2];
        vec.push_back((
            tuid.parse::<u64>().unwrap(),
            lat.parse::<f64>().unwrap(),
            lon.parse::<f64>().unwrap()
        ));
    }
    Ok(())
}

pub fn random_RIR_25k() -> Result<(),CsvError>{
    let configs = rtree_IBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 30_000,
        bin_volumn: 30_000,
        collection_volumn: 24999,
        buffer_volumn: 1
    };
    let file_path = "./data/sample-data/random/".to_string();
    let output_record_file_name = "./data/log/random_RIR_25k.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);

    
    for _cnt in 1..101 {
        let file_name = format!("random-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(1000_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            let temp_node = rtree_IBR:: Node::new(
                rtree_IBR::Rect::new(rtree_IBR::Point::new([_t.1,_t.2]), rtree_IBR::Point::new([_t.1,_t.2])),
                Vec::with_capacity(0),
                0,
                false,
                0,
                _t.0
            );
            rt.insert(temp_node);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: random RIR 25k.");
    Ok(())
}

pub fn random_RIR_1M() -> Result<(),CsvError>{
    let configs = rtree_IBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 1000_000,
        bin_volumn: 1000_000,
        collection_volumn: 999_999,
        buffer_volumn: 1
    };
    let file_path = "./data/sample-data/random/".to_string();
    let output_record_file_name = "./data/log/random_RIR_1M.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);

    
    for _cnt in 1..101 {
        let file_name = format!("random-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(1000_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            let temp_node = rtree_IBR:: Node::new(
                rtree_IBR::Rect::new(rtree_IBR::Point::new([_t.1,_t.2]), rtree_IBR::Point::new([_t.1,_t.2])),
                Vec::with_capacity(0),
                0,
                false,
                0,
                _t.0
            );
            rt.insert(temp_node);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: random RIR 1M.");
    Ok(())
}

pub fn ADSB_RIR_25k() -> Result<(),CsvError>{
    let configs = rtree_IBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 30_000,
        bin_volumn: 30_000,
        collection_volumn: 24999,
        buffer_volumn: 1
    };
    let file_path = "./data/sample-data/ADSB/".to_string();
    let output_record_file_name = "./data/log/ADSB_RIR_25k.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);

    
    for _cnt in 1..101 {
        let file_name = format!("ADSB-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(1000_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            let temp_node = rtree_IBR:: Node::new(
                rtree_IBR::Rect::new(rtree_IBR::Point::new([_t.1,_t.2]), rtree_IBR::Point::new([_t.1,_t.2])),
                Vec::with_capacity(0),
                0,
                false,
                0,
                _t.0
            );
            rt.insert(temp_node);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: ADSB RIR 25k.");
    Ok(())
}

pub fn ADSB_RIR_1M() -> Result<(),CsvError>{
    let configs = rtree_IBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 1000_000,
        bin_volumn: 1000_000,
        collection_volumn: 999_999,
        buffer_volumn: 1
    };
    let file_path = "./data/sample-data/ADSB/".to_string();
    let output_record_file_name = "./data/log/ADSB_RIR_1M.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);

    
    for _cnt in 1..101 {
        let file_name = format!("ADSB-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(1000_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            let temp_node = rtree_IBR:: Node::new(
                rtree_IBR::Rect::new(rtree_IBR::Point::new([_t.1,_t.2]), rtree_IBR::Point::new([_t.1,_t.2])),
                Vec::with_capacity(0),
                0,
                false,
                0,
                _t.0
            );
            rt.insert(temp_node);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: ADSB RIR 1M.");
    Ok(())
}

pub fn AIS_RIR_25k() -> Result<(),CsvError>{
    let configs = rtree_IBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 30_000,
        bin_volumn: 30_000,
        collection_volumn: 24999,
        buffer_volumn: 1
    };
    let file_path = "./data/sample-data/AIS/".to_string();
    let output_record_file_name = "./data/log/AIS_RIR_25k.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);

    
    for _cnt in 1..101 {
        let file_name = format!("AIS-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(1000_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            let temp_node = rtree_IBR:: Node::new(
                rtree_IBR::Rect::new(rtree_IBR::Point::new([_t.1,_t.2]), rtree_IBR::Point::new([_t.1,_t.2])),
                Vec::with_capacity(0),
                0,
                false,
                0,
                _t.0
            );
            rt.insert(temp_node);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: AIS RIR 25k.");
    Ok(())
}

pub fn AIS_RIR_1M() -> Result<(),CsvError>{
    let configs = rtree_IBR::ConfigParams{
        max_items: 32,
        min_items: 6,
        capacity: 1000_000,
        bin_volumn: 1000_000,
        collection_volumn: 999_999,
        buffer_volumn: 1
    };
    let file_path = "./data/sample-data/AIS/".to_string();
    let output_record_file_name = "./data/log/AIS_RIR_1M.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);

    
    for _cnt in 1..101 {
        let file_name = format!("AIS-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(1000_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            let temp_node = rtree_IBR:: Node::new(
                rtree_IBR::Rect::new(rtree_IBR::Point::new([_t.1,_t.2]), rtree_IBR::Point::new([_t.1,_t.2])),
                Vec::with_capacity(0),
                0,
                false,
                0,
                _t.0
            );
            rt.insert(temp_node);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: AIS RIR 1M.");
    Ok(())
}
