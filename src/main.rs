
use cap::Cap;
use std::{thread, time};
use std::alloc;
use std::fs;
#[global_allocator]
static ALLOCATOR: Cap<alloc::System> = Cap::new(alloc::System, usize::max_value());
mod test_RIR;
mod test_RIBR;
mod test_tiRIBR;

mod test_R_Rstar;

mod test_write;
mod test_search;
pub mod test_write_round;
pub mod test_search_round;

fn clear_dir(dir:String) {
    let path = dir;
    fs::remove_dir_all(path.clone()).unwrap();
    fs::create_dir(path.clone()).unwrap();
    let one_second = time::Duration::from_millis(1000);
    thread::sleep(one_second);
}
fn test_crossbeam_random_main() {
    let thread_numbers = [1,3,6,9,12,15];
    for n_workers in thread_numbers {
        clear_dir("data/index/random/1M-25".to_string());
        test_write_round::crossbeam_random_tiRIBR_1M_25(n_workers);
        test_search_round::crossbeam_random_tiRIBR_1M_25_search_tp10(n_workers);
        test_search_round::crossbeam_random_tiRIBR_1M_25_search_tp100(n_workers);
        clear_dir("data/index/random/1M-25".to_string());
    }
    for n_workers in thread_numbers {
        clear_dir("data/index/random/25k-25".to_string());
        test_write_round::crossbeam_random_tiRIBR_25k_25(n_workers);
        test_search_round::crossbeam_random_tiRIBR_25k_25_search_tp10(n_workers);
        test_search_round::crossbeam_random_tiRIBR_25k_25_search_tp100(n_workers);
        clear_dir("data/index/random/25k-25".to_string());
    }
}

fn test_crossbeam_ADSB_main () {
    let thread_numbers = [1,3,6,9,12,15];
    for n_workers in thread_numbers {
        clear_dir("data/index/ADSB/25k-25".to_string());
        test_write_round::crossbeam_ADSB_tiRIBR_25k_25(n_workers);
        test_search_round::crossbeam_ADSB_tiRIBR_25k_25_search_tp10(n_workers);
        test_search_round::crossbeam_ADSB_tiRIBR_25k_25_search_tp100(n_workers);
        clear_dir("data/index/ADSB/25k-25".to_string());
    }
    for n_workers in thread_numbers {
        clear_dir("data/index/ADSB/1M-25".to_string());
        test_write_round::crossbeam_ADSB_tiRIBR_1M_25(n_workers);
        test_search_round::crossbeam_ADSB_tiRIBR_1M_25_search_tp10(n_workers);
        test_search_round::crossbeam_ADSB_tiRIBR_1M_25_search_tp100(n_workers);
        clear_dir("data/index/ADSB/1M-25".to_string());
    }
}

fn test_crossbeam_AIS_main () {
    let thread_numbers = [3,1,6,9,12,15];
    for n_workers in thread_numbers {
        clear_dir("data/index/AIS/25k-25".to_string());
        test_write_round::crossbeam_AIS_tiRIBR_25k_25(n_workers);
        test_search_round::crossbeam_AIS_tiRIBR_25k_25_search_tp10(n_workers);
        test_search_round::crossbeam_AIS_tiRIBR_25k_25_search_tp100(n_workers);
        clear_dir("data/index/AIS/25k-25".to_string());
    }
    for n_workers in thread_numbers {
        clear_dir("data/index/AIS/1M-25".to_string());
        test_write_round::crossbeam_AIS_tiRIBR_1M_25(n_workers);
        test_search_round::crossbeam_AIS_tiRIBR_1M_25_search_tp10(n_workers);
        test_search_round::crossbeam_AIS_tiRIBR_1M_25_search_tp100(n_workers);
        clear_dir("data/index/AIS/1M-25".to_string());
    }
}

fn main(){
    //test_crossbeam_random_main(); 
    //test_crossbeam_ADSB_main();
    //test_crossbeam_AIS_main();
    
    //test_search::crossbeam_AIS_tiRIBR_25k_25_search_tp10();
    //test_search::crossbeam_AIS_tiRIBR_25k_25_search_tp100();
    //test_search::crossbeam_AIS_tiRIBR_1M_25_search_tp10();
    //test_search::crossbeam_AIS_tiRIBR_1M_25_search_tp100();

    //test_write:: crossbeam_AIS_tiRIBR_25k_25();
    //test_write:: crossbeam_AIS_tiRIBR_1M_25();

    //test_search::crossbeam_ADSB_tiRIBR_25k_25_search_tp10();
    //test_search::crossbeam_ADSB_tiRIBR_25k_25_search_tp100();
    //test_search::crossbeam_ADSB_tiRIBR_1M_25_search_tp10();
    //test_search::crossbeam_ADSB_tiRIBR_1M_25_search_tp100();
    //test_write:: crossbeam_ADSB_tiRIBR_25k_25();
    //test_write:: crossbeam_ADSB_tiRIBR_1M_25();

    //test_search::crossbeam_random_tiRIBR_25k_25_search_tp10();
    //test_search::crossbeam_random_tiRIBR_25k_25_search_tp100();
    //test_search::crossbeam_random_tiRIBR_1M_25_search_tp10();
    //test_search::crossbeam_random_tiRIBR_1M_25_search_tp100();
    //test_write::crossbeam_random_tiRIBR_25k_25();
    //test_write:: crossbeam_random_tiRIBR_1M_25();

    //test_RIR:: random_RIR_25k();
    //test_RIBR:: random_RIBR_25k_1();
    //test_RIBR:: random_RIBR_25k_10();
    //test_RIBR:: random_RIBR_25k_25();
    //test_tiRIBR:: random_tiRIBR_25k_1();
    //test_tiRIBR:: random_tiRIBR_25k_10();
    //test_tiRIBR:: random_tiRIBR_25k_25();
    //test_RIR:: random_RIR_1M();
    //test_RIBR:: random_RIBR_1M_1();
    //test_RIBR:: random_RIBR_1M_10();
    //test_RIBR:: random_RIBR_1M_25();
    //test_tiRIBR:: random_tiRIBR_1M_1();
    //test_tiRIBR:: random_tiRIBR_1M_10();
    //test_tiRIBR:: random_tiRIBR_1M_25();
    
    //test_RIR:: ADSB_RIR_25k();
    //test_RIBR:: ADSB_RIBR_25k_1();
    //test_RIBR:: ADSB_RIBR_25k_10();
    //test_RIBR:: ADSB_RIBR_25k_25();
    //test_tiRIBR:: ADSB_tiRIBR_25k_1();
    //test_tiRIBR:: ADSB_tiRIBR_25k_10();
    //test_tiRIBR:: ADSB_tiRIBR_25k_25();
    //test_RIR:: ADSB_RIR_1M();
    //test_RIBR:: ADSB_RIBR_1M_1();
    //test_RIBR:: ADSB_RIBR_1M_10();
    //test_RIBR:: ADSB_RIBR_1M_25();
    //test_tiRIBR:: ADSB_tiRIBR_1M_1();
    //test_tiRIBR:: ADSB_tiRIBR_1M_10();
    //test_tiRIBR:: ADSB_tiRIBR_1M_25();

    //test_RIR:: AIS_RIR_25k();
    //test_RIBR:: AIS_RIBR_25k_1();
    //test_RIBR:: AIS_RIBR_25k_10();
    //test_RIBR:: AIS_RIBR_25k_25();
    //test_tiRIBR:: AIS_tiRIBR_25k_1();
    //test_tiRIBR:: AIS_tiRIBR_25k_10();
    //test_tiRIBR:: AIS_tiRIBR_25k_25();
    //test_RIR:: AIS_RIR_1M();
    //test_RIBR:: AIS_RIBR_1M_1();
    //test_RIBR:: AIS_RIBR_1M_10();
    //test_RIBR:: AIS_RIBR_1M_25();
    //test_tiRIBR:: AIS_tiRIBR_1M_1();
    //test_tiRIBR:: AIS_tiRIBR_1M_10();
    //test_tiRIBR:: AIS_tiRIBR_1M_25();
    
    //test_R_Rstar::random_R();
    //test_R_Rstar::ADSB_R();
    //test_R_Rstar::AIS_R();
    //test_R_Rstar::random_Rstar();
    //test_R_Rstar::ADSB_Rstar();
    //test_R_Rstar::AIS_Rstar();
    //test_R_Rstar::random_Kd();
    //test_R_Rstar::ADSB_Kd();
    //test_R_Rstar::AIS_Kd();
}


