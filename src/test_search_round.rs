use mylib::rtree_tiIBR;

//mod rtree_tiIBR;
// mod tirtree;
//
use rand;
use std::fs;
use csv::{Reader, Writer, Error as CsvError};
use rand::seq::index;

use std::collections::VecDeque;
use std::sync::mpsc::channel;
use std::time::{Duration, Instant};
use std::fmt::{Display, format};
use std::fs::File;
use std::io::prelude::*;
use std::io::{Write, BufReader, BufRead, Error};
use crate::ALLOCATOR;

use crossbeam;
use crossbeam_channel::{self, unbounded};
use crossbeam_channel::bounded;

fn count_range_query_with_time (rt: &rtree_tiIBR::Rtree<2>, min_t:u64, max_t:u64, rect: &rtree_tiIBR::Rect<2>) -> usize{
    let res = rt.search(rect.clone());
    let mut cnt = 0;
    for i in 0..res.len(){
        if res[i].tuid() >= min_t && res[i].tuid() <= max_t {
            cnt += 1;
        }
    }
    cnt
}

pub fn crossbeam_query (dataset: String, suffix: String, tp: usize,n_workers:usize, max_num: usize) -> Result<(),CsvError> {
    // file names prepare
    let mut files: Vec<String> = vec![];
    let file_path = format!("./data/sample-data/{}/", dataset).to_string();
    let output_record_file_name = format!("./data/log-search/{}-{}-tiRIBR-{}-tp{}.csv", n_workers,dataset, suffix, tp).to_string();
    let output_file_path = format!("./data/log-search/").to_string();

    // prepare query data
    let mut query_vec  = vec![];
    let query_file = format!("./data/query/{}-query-tp{}.csv", dataset, tp).to_string();
    let mut reader = Reader::from_path(query_file)?;
    for rec in reader.records(){
        let record = rec?;
        let min_time = &record[0];
        let max_time = &record[1];
        let min_lat = &record[2];
        let max_lat = &record[3];
        let min_lon = &record[4];
        let max_lon = &record[5];

        query_vec.push((
            min_time.parse::<u64>().unwrap(),
            max_time.parse::<u64>().unwrap(),
            min_lat.parse::<f64>().unwrap(),
            min_lon.parse::<f64>().unwrap(),
            max_lat.parse::<f64>().unwrap(),
            max_lon.parse::<f64>().unwrap()
        ));
    }

    let (snd,rcv) = unbounded();
    let start = Instant::now();
    // parallel process begin
    let _ = crossbeam::scope(|s| {

        for _w_i in 0..n_workers {
            let (sndr, rcvr) = (snd.clone(), rcv.clone());
            let the_dataset = dataset.clone();
            let the_tp = tp.clone();
            let the_suffix = suffix.clone();
            let the_output_file_path = output_file_path.clone();
            let the_query_vec = query_vec.clone();
            
            let output_file = format!("{}{}-tp{}-{}-{}-search-result.csv", the_output_file_path,the_dataset,the_tp,the_suffix,_w_i);

            let index_file_path = format!("./data/index/{}/{}/",the_dataset,the_suffix).to_string();

            s.spawn(move |_| { 
                let read_start =Instant::now();
                let mut rtrees = vec![];
                for _r_c in 1..max_num{
                    // let path = format!("{}{}-{}-{}-{}.json", the_output_file_path,n_workers,the_dataset,_w_i,write_cnt);
                    let index_file = format!("{}{}-{}-{}-{}.json", index_file_path,n_workers,the_dataset,_w_i,_r_c);
                    if let Ok(mut file) = File::open(index_file)  {
                        let mut contents = String::new();
                        file.read_to_string(&mut contents);
                        let mut deser:rtree_tiIBR::Rtree<2> = serde_json::from_str(&contents).unwrap();
                        rtrees.push(deser);
                    }else {
                        break;
                    }
                    
                }
                
                let read_duration = format!("{:?}",read_start.elapsed().as_nanos());
                let calc_start = Instant::now();

                let mut res_wtr = Writer::from_path(output_file).unwrap();
                for _i in 0..the_query_vec.len(){
                    let mut res = 0;
                    let q = the_query_vec[_i].clone();
                    let min_t = q.0;
                    let max_t = q.1;
                    let rc = rtree_tiIBR::Rect::new(
                                        rtree_tiIBR::Point::new([q.2,q.3]), 
                                        rtree_tiIBR::Point::new([q.4,q.5]));
                    for _j in 0..rtrees.len(){
                        let rt = &rtrees[_j];
                        let rt_min_tuid = rt.time_min();
                        let rt_max_tuid = rt.time_max();
                        if rt_max_tuid < min_t || rt_min_tuid > max_t{
                            continue;
                        }
                        res += count_range_query_with_time(rt, min_t, max_t, &rc);
                    }
                    
                    res_wtr.write_record(&[_i.to_string(),res.to_string()]);     
                    res_wtr.flush();
                }
                println!("End:{}", _w_i);

                let calc_duration = format!("{:?}",calc_start.elapsed().as_nanos());
                sndr.send((_w_i,read_duration,calc_duration));
                
            });
        }
        drop(snd);
        
        
        let mut wtr = Writer::from_path(output_record_file_name).unwrap();
        for msg in rcv.iter(){
            let (w_i, read_duration , calc_duration) = msg;
            wtr.write_record(&[w_i.to_string(),read_duration,calc_duration]).unwrap();     
        }
        wtr.flush().unwrap();
    });
    // parallel process end
    println!("End: main");
    
    Ok(())
}

pub fn crossbeam_random_tiRIBR_25k_25_search_tp10 (n_workers:usize){
    let dataset = "random".to_string();
    let suffix = "25k-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 10, n_workers, 800);

}
pub fn crossbeam_random_tiRIBR_25k_25_search_tp100 (n_workers:usize){
    let dataset = "random".to_string();
    let suffix = "25k-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 100, n_workers,800);
}

pub fn crossbeam_random_tiRIBR_1M_25_search_tp10 (n_workers:usize){
    let dataset = "random".to_string();
    let suffix = "1M-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 10, n_workers, 800);

}

pub fn crossbeam_random_tiRIBR_1M_25_search_tp100 (n_workers:usize){
    let dataset = "random".to_string();
    let suffix = "1M-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 100, n_workers,800);

}


pub fn crossbeam_ADSB_tiRIBR_25k_25_search_tp10 (n_workers:usize){
    let dataset = "ADSB".to_string();
    let suffix = "25k-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 10, n_workers, 800);

}
pub fn crossbeam_ADSB_tiRIBR_25k_25_search_tp100 (n_workers:usize){
    let dataset = "ADSB".to_string();
    let suffix = "25k-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 100, n_workers, 800);
}

pub fn crossbeam_ADSB_tiRIBR_1M_25_search_tp10 (n_workers:usize){
    let dataset = "ADSB".to_string();
    let suffix = "1M-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 10, n_workers, 820);

}

pub fn crossbeam_ADSB_tiRIBR_1M_25_search_tp100 (n_workers:usize){
    let dataset = "ADSB".to_string();
    let suffix = "1M-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 100, n_workers, 820);

}

pub fn crossbeam_AIS_tiRIBR_25k_25_search_tp10 (n_workers:usize){
    let dataset = "AIS".to_string();
    let suffix = "25k-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 10,n_workers, 800);

}
pub fn crossbeam_AIS_tiRIBR_25k_25_search_tp100 (n_workers:usize){
    let dataset = "AIS".to_string();
    let suffix = "25k-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 100, n_workers,800);
}

pub fn crossbeam_AIS_tiRIBR_1M_25_search_tp10 (n_workers:usize){
    let dataset = "AIS".to_string();
    let suffix = "1M-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 10, n_workers,820);

}

pub fn crossbeam_AIS_tiRIBR_1M_25_search_tp100 (n_workers:usize){
    let dataset = "AIS".to_string();
    let suffix = "1M-25".to_string();
    let _ = crossbeam_query(dataset, suffix, 100, n_workers , 820);

}