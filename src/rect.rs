
use std::cmp::Ordering;

use serde::{Deserialize, Serialize};
use serde_big_array::BigArray;
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Point<const D:usize>{
    #[serde(with = "BigArray")]
    pub coords: [f64;D]
}
#[derive(Clone, Deserialize, Serialize)]
pub struct Rect<const D:usize>{
    pub min: Point<D>,
    pub max: Point<D>
}

impl <const D:usize> Point<D> {
    pub fn new(array:[f64;D]) -> Point<D>{
        Point{coords:array}
    }
}
impl <const D:usize> Rect<D>{
    pub fn new_empty_rect() -> Rect<D>{
        Rect{ min: Point{coords:[0.0;D]}, max: Point{coords:[0.0;D]}}
    }
    pub fn new(p1:Point<D>,p2:Point<D>) -> Rect<D>{
        let mut min = [0.0;D];
        let mut max = [0.0;D];
        for _i in 0..D{
            if p1.coords[_i] <= p2.coords[_i] {
                min[_i] = p1.coords[_i];
                max[_i] = p2.coords[_i];
            }
            else{
                min[_i] = p2.coords[_i];
                max[_i] = p1.coords[_i];
            }
        }
        Rect{ min: Point{coords:min}, max: Point{coords:max}}
    }
    pub fn expand(&mut self, rect: &Rect<D>){
       for _i in 0..D{
            if rect.min.coords[_i] < self.min.coords[_i] {
                self.min.coords[_i] = rect.min.coords[_i];
            } 
            if rect.max.coords[_i] > self.max.coords[_i] {
                self.max.coords[_i] = rect.max.coords[_i];
            }
       } 
    }
    pub fn area(&self) -> f64 {
        if D==0 {
            return 0.0;
        }
        let mut area = self.max.coords[0]-self.min.coords[0];
        for _i in 1..D {
            area = area * (self.max.coords[_i]-self.min.coords[_i]);
        }
        area
    }
    pub fn unioned_area(&self, rect: &Rect<D>) -> f64 {
        let mut r = self.clone();
        r.expand(rect);
        r.area()
    }
    pub fn largest_axis(&self) -> usize {
        if D == 0 {
            return 0;
        }
        let mut axis = 0;
        let mut length = self.max.coords[0] - self.min.coords[0];
        for _i in 1..D {
            let temp_len = self.max.coords[_i] - self.min.coords[_i];
            if temp_len > length {
                axis = _i;
                length = temp_len;
            }
        }
        axis
    }
    pub fn cmp_by_axis_min(rect1: &Rect<D>, rect2: &Rect<D>, axis:usize) -> Ordering{
        if rect1.min.coords[axis] < rect2.min.coords[axis]{
            return Ordering::Less;
        }
        else if rect1.min.coords[axis] > rect2.min.coords[axis]{
            return Ordering::Greater;
        }
        else{
            return Ordering::Equal;
        }
    }
    pub fn cmp_by_axis_max(rect1: &Rect<D>, rect2: &Rect<D>, axis:usize) -> Ordering{
        if rect1.max.coords[axis] < rect2.max.coords[axis]{
            return Ordering::Less;
        }
        else if rect1.max.coords[axis] > rect2.max.coords[axis]{
            return Ordering::Greater;
        }
        else{
            return Ordering::Equal;
        }
    }
}


#[test]
fn test_point_rect() {
    let p1 = Point{coords:[435.0,34.0]};
    let p2 = Point{coords:[23.0,342.0]};
    let p3 = Point{coords:[23.0,34.0]};
    let p4 = Point{coords:[435.0,342.0]};
    println!("{}",serde_json::to_string(&p1).unwrap());
    let r1 = Rect::new(p1,p2);
    let r2=Rect::new(p3, p4); 
    println!("{}",serde_json::to_string(&r1).unwrap());
    println!("{}",serde_json::to_string(&r2).unwrap());
}

