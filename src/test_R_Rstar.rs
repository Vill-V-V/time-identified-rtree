use rand;
use csv::{Reader, Writer, Error as CsvError};

use rtree_rs;
use rstar;
use kdtree::KdTree;

use std::collections::VecDeque;
use std::time::{Duration, Instant};
use std::fmt::Display;
use std::fs::File;

use crate::ALLOCATOR;

fn read_csv_file(file_path:&String,  file_name:&String, vec: &mut VecDeque<(u64,f64,f64)>) -> Result<(),CsvError> {
    let mut reader = Reader::from_path(format!("{}{}",file_path, file_name))?;
    for rec in reader.records(){
        let record = rec?;
        let tuid = &record[0];
        let lat = &record[1];
        let lon = &record[2];
        vec.push_back((
            tuid.parse::<u64>().unwrap(),
            lat.parse::<f64>().unwrap(),
            lon.parse::<f64>().unwrap()
        ));
    }
    Ok(())
}

pub fn random_R () -> Result<(),CsvError> {

    
    let file_path = "./data/sample-data/random/".to_string();
    let output_record_file_name = "./data/log/random_R.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    //let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);
    let mut tr = rtree_rs::RTree::new();
    
    for _cnt in 1..101 {
        let file_name = format!("random-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(500_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            tr.insert(rtree_rs::Rect::new([_t.1,_t.2],[_t.1,_t.2]), _t.0);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: random R.");
    Ok(())
}
pub fn ADSB_R () -> Result<(),CsvError> {

    
    let file_path = "./data/sample-data/ADSB/".to_string();
    let output_record_file_name = "./data/log/ADSB_R.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    //let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);
    let mut tr = rtree_rs::RTree::new();
    
    for _cnt in 1..101 {
        let file_name = format!("ADSB-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(500_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            tr.insert(rtree_rs::Rect::new([_t.1,_t.2],[_t.1,_t.2]), _t.0);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: ADSB R.");
    Ok(())
}
pub fn AIS_R () -> Result<(),CsvError> {

    
    let file_path = "./data/sample-data/AIS/".to_string();
    let output_record_file_name = "./data/log/AIS_R.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    //let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);
    let mut tr = rtree_rs::RTree::new();
    
    for _cnt in 1..101 {
        let file_name = format!("AIS-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(500_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            tr.insert(rtree_rs::Rect::new([_t.1,_t.2],[_t.1,_t.2]), _t.0);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: AIS R.");
    Ok(())
}

pub fn random_Rstar () -> Result<(),CsvError> {

    
    let file_path = "./data/sample-data/random/".to_string();
    let output_record_file_name = "./data/log/random_Rstar.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    //let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);
    //let mut tr = rtree_rs::RTree::new();
    let mut tr = rstar::RTree::new();
    for _cnt in 1..101 {
        let file_name = format!("random-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(500_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            tr.insert([_t.1,_t.2]);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: random Rstar.");
    Ok(())
}

pub fn ADSB_Rstar () -> Result<(),CsvError> {

    
    let file_path = "./data/sample-data/ADSB/".to_string();
    let output_record_file_name = "./data/log/ADSB_Rstar.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    //let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);
    //let mut tr = rtree_rs::RTree::new();
    let mut tr = rstar::RTree::new();
    for _cnt in 1..101 {
        let file_name = format!("ADSB-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(500_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            tr.insert([_t.1,_t.2]);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: ADSB Rstar.");
    Ok(())
}

pub fn AIS_Rstar () -> Result<(),CsvError> {

    
    let file_path = "./data/sample-data/AIS/".to_string();
    let output_record_file_name = "./data/log/AIS_Rstar.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    //let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);
    //let mut tr = rtree_rs::RTree::new();
    let mut tr = rstar::RTree::new();
    for _cnt in 1..101 {
        let file_name = format!("AIS-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(500_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            tr.insert([_t.1,_t.2]);
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: AIS Rstar.");
    Ok(())
}

pub fn random_Kd () -> Result<(),CsvError> {

    
    let file_path = "./data/sample-data/random/".to_string();
    let output_record_file_name = "./data/log/random_Kd.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    //let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);
    //let mut tr = rtree_rs::RTree::new();
    
    let dimensions = 2;
    let mut tr = KdTree::new(dimensions);
    for _cnt in 1..101 {
        let file_name = format!("random-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(500_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            tr.add([_t.1,_t.2],_i).unwrap();
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: random Kd.");
    Ok(())
}

pub fn ADSB_Kd () -> Result<(),CsvError> {

    
    let file_path = "./data/sample-data/ADSB/".to_string();
    let output_record_file_name = "./data/log/ADSB_Kd.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    //let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);
    //let mut tr = rtree_rs::RTree::new();
    let dimensions = 2;
    let mut tr = KdTree::new(dimensions);
    for _cnt in 1..101 {
        let file_name = format!("ADSB-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(500_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            tr.add([_t.1,_t.2],_i).unwrap();
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: ADSB Kd.");
    Ok(())
}

pub fn AIS_Kd () -> Result<(),CsvError> {

    
    let file_path = "./data/sample-data/AIS/".to_string();
    let output_record_file_name = "./data/log/AIS_Kd.csv";
    let mut wtr = Writer::from_path(output_record_file_name)?;
    
    //let mut rt:rtree_IBR::Rtree<2> = rtree_IBR::Rtree::new(configs);
    //let mut tr = rtree_rs::RTree::new();
    let dimensions = 2;
    let mut tr = KdTree::new(dimensions);
    for _cnt in 1..101 {
        let file_name = format!("AIS-{}.csv", _cnt).to_string();
        
        let mut tuple_vec = VecDeque::with_capacity(500_000);
        
        read_csv_file(&file_path, &file_name, &mut tuple_vec);
        
        let start = Instant::now();
        for _i in 0..tuple_vec.len() {
            let _t = tuple_vec[_i];
            tr.add([_t.1,_t.2],_i).unwrap();
        }
        let duration = format!("{:?}",start.elapsed().as_nanos());
        let alloc_rec = format!("{}",ALLOCATOR.allocated());
        

        wtr.write_record(&[alloc_rec,duration])?;     
        wtr.flush()?;
    }

    println!("End: AIS Kd.");
    Ok(())
}